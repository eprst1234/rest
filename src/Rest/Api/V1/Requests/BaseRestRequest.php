<?php

namespace Rest\Services\Api\V1\Requests;

use App\Http\Requests\RequestInterface;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\Request;

class BaseRestRequest extends Request implements RequestInterface
{
    /**
    * Determine if the user is authorized to make this request.
    *
    * @return bool
    */
    public function authorize()
    {
        return true;
    }

    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules()
    {
		//$this->formatInput();
		
        if($this->input('action') == 'changeStatus'){
            return [
                'id' => 'required|integer'
            ];
        }
		
        if($this->getRules()){
            return $this->getRules();
        }
		
        return [];
    }

    /**
     * 
     */
    public function getRules()
    {
        
    }

    /**
     * @param array $errors
     * @return JsonResponse
     */
    public function response(array $errors)
    {
        return new JsonResponse(['result' => 'error', 'errors' => $errors]);
    }
}
