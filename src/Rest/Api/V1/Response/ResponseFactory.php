<?php

namespace Rest\Services\Api\V1\Response;

use Closure;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;

class ResponseFactory
{
    protected $transformer;
	
	public function collection(Collection $collection, $transformer, $parameters = [], Closure $after = null)
    {
        if ($collection->isEmpty()) {
            $class = get_class($collection);
        } else {
            $class = get_class($collection->first());
        }

        if ($parameters instanceof \Closure) {
            $after = $parameters;
            $parameters = [];
        }

        return new Response($collection, 200, [], $transformer);
    }
	
	public function paginator(Paginator $paginator, $transformer, array $parameters = [], Closure $after = null)
    {
        if ($paginator->isEmpty()) {
            $class = get_class($paginator);
        } else {
            $class = get_class($paginator->first());
        }

        return new Response($paginator, 200, [], $transformer);
    }
	
	public function item($item, $transformer, $parameters = [], Closure $after = null)
    {
        $class = get_class($item);

        if ($parameters instanceof \Closure) {
            $after = $parameters;
            $parameters = [];
        }

        return new Response($item, 200, [], $transformer);
    }
	
}
