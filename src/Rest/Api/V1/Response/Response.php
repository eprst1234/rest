<?php

namespace Rest\Services\Api\V1\Response;

use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class Response extends IlluminateResponse
{
	protected static $transformer;
	
	protected $meta = [];
	
	
	public function __construct($content, $status = 200, $headers = [], $transformer)
    {
        parent::__construct($content, $status, $headers);
		static::$transformer = $transformer;
    }
	
	public function send(){
		$this->morph();
		parent::send();
	}

	public function morph($format = 'json')
    {
        $this->content = $this->getOriginalContent();
		$arr = [];
		$arr['meta'] = [];
		$newArr = [];
		if ($this->content instanceof LengthAwarePaginator) {
			$items = $this->content->getCollection();
			foreach($items as $item){
				$newArr[] = static::$transformer->transform($item);
			}
			$arr['data'] = $newArr;
			$this->addMeta('pagination', ['current_page'=>$this->content->currentPage(), 'total_pages' => $this->content->lastPage()]);
		}else if($this->content instanceof Collection){
			foreach($this->content as $item){
				$newArr[] = static::$transformer->transform($item);
			}
			$arr['data'] = $newArr;
		}else{
			$newArr = static::$transformer->transform($this->content);
			$arr['data'] = $newArr;
		}
		
		$this->setMeta($arr);
		
		$this->content = json_encode($arr);
		
        return $this;
    }
	
    public function addMeta($key, $value)
    {
        $this->meta[$key]=$value;
		return $this;
    }
	
	protected function setMeta(&$arr){
		foreach($this->meta as $key=>$val){
			$arr['meta'][$key] = $val;
		}
	}
}
