<?php

namespace Rest\Services\Api\V1\Controllers;

use Rest\Services\Api\V1\Response\ResponseFactory as BaseResponse;
use App\Models\Base\BaseModel;
use Carbon\Carbon;
use Doctrine\DBAL\Query\QueryBuilder;
use Schema;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\RequestInterface;
use App\Http\Controllers\Controller;
use Cache;
use Auth;
use App\Helpers\Crypt;
use Session;


abstract class RestController extends Controller
{
    /**
    * @var bool
    */
    public $sofDelete = false;
    /**
    * @var string
    */
    public $modelName = '';
    /**
    * @var QueryBuilder
    */
    public $modelQuery;
    /**
     * @var bool
     */
    public $modelTableAlias = false;
    /**
    * @var string
    */
    public $transformer = '';
    /**
    * @var int
    */
    public $perPage = 25;
    /**
    * @var string
    */
    public $successCreateText;
    /**
    * @var string
    */
    public $errorCreateText;
    /**
    * @var string
    */
    public $successUpdateText;
    /**
    * @var string
    */
    public $errorUpdateText;
    /**
    * @var string
    */
    public $successDeleteText;
    /**
    * @var string
    */
    public $errorDeleteText;
    /**
    * @var Request
    */
    public $request;
    /**
    * @var string
    */
    public $cointrollerName;
    /**
    * @var string
    */
    public $actionName;
    /**
     * @var string
     */
    public $cacheSortByPart = '';
    /**
     * @var array
     */
    public $onlyFieldsCreate = [];
    /**
     * @var array
     */
    public $onlyFieldsUpdate = [];
    /**
     * @var array
     */
	public $baseOnlyFields = [];

    /**
     * RestController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
		//Session::forget('pass');

        if(!app()->runningInConsole()){
            $rolePaths = explode('/', $request->route()->getPath());
            //Auth::loginUsingId(1);

//            if(isset($authUser) && isset($rolePaths[3]) && ($authUser->mainRole()->slug === $rolePaths[3] || $rolePaths[3] === "base")){
//                //TODO
//            }else{
//                throw new \Exception('Not correct request role');
//            }

            $currentAction = $request->route()->getAction()['uses'];
            list($controller, $method) = explode('@', $currentAction);
            $this->cointrollerName = preg_replace('/.*\\\/', '', $controller);
            $this->actionName = preg_replace('/.*\\\/', '', $method);
        }

       // $this->request = $request;
		$this->decryptData($request);

        $model = new $this->modelName;
        $columnListing = Schema::getColumnListing($model->getTable());

        $this->prepareFields();
        $this->prepareWith();
		$this->prepareHas();
		$this->prepareNotIn();
        $this->prepareOrderBy($columnListing);
        $this->prepareGroupBy($columnListing);
		$this->prepareLike($columnListing);
		$this->prepareRange($columnListing);
        if($this->modelTableAlias){
            $this->modelQuery->from($model->getTable() . ' AS ' . $this->modelTableAlias);
        }

        $this->softDeleteCondition($request);
        $this->prepareTransformers($request);
    }
	
	
	public function response(){
		return app(BaseResponse::class);
	}
	
	protected function user()
    {
        return Auth::user();
    }

    /**
     * @param $request
     */
	public function decryptData($request){
		if($request->method() != 'DELETE' && config('app.cryptoRequest')){
			$arr = $request->all();
			//dd($arr);
			$this->recursiveCrypto($arr);

			$request->replace($arr);
		}
		$this->request = $request;
	}

    /**
     * @param $arr
     */
	public function recursiveCrypto(&$arr){

		if(is_array($arr)){
			foreach($arr as $key => &$val){
				$this->recursiveCrypto($val);
			}
		}else{
			$arr = Crypt::decrypt($arr, Session::get('pass'));
		}
	}

    /**
     *
     */
	public function unsetRelations()
    {

	}
    /**
     * @param RequestInterface $requestInterface
     * @return Response
     * @throws \Exception
     */
    public function index(RequestInterface $requestInterface)
    {
        $this->unsetRelations();
        $this->indexCallback($requestInterface);

        if ($requestInterface->has('paginate') && ($requestInterface->get('paginate') == 'false')) {
            return $this->response()
                    ->collection(collect($this->modelQuery->get()->all()), new $this->transformer)
                    ->addMeta('result', 'success');
        }


        $cacheKey = $this->getCacheKey();
/*        if (Cache::has($cacheKey)) {
            $result = Cache::get($cacheKey);
        } else {
            $result = $this->modelQuery->paginate($this->perPage);
            Cache::forever($cacheKey, $result);
        }
*/

        $result = $this->modelQuery->paginate($this->perPage);

	  // $result = $this->modelQuery->toSql();
		//dd($result);
		//$response = $this->response()->paginator($result, new $this->transformer)->addMeta('result', 'success')->addMeta('cache_key', $cacheKey);
        return $this->indexResponse($result, $cacheKey);
    }

    /**
     * @param $result
     * @param $cacheKey
     * @return Response
     */
	public function indexResponse($result, $cacheKey)
    {
		return $this->response()->paginator($result, new $this->transformer)->addMeta('result', 'success')->addMeta('cache_key', $cacheKey);
	}

    /**
     * @param RequestInterface $requestInterface
     */
    public function indexCallback(RequestInterface $requestInterface)
    {

    }
    /**
     * @return array
     */
    public function onlyFieldsCreate($requestInterface)
    {
		if(count($this->onlyFieldsCreate) == 0){
			return $requestInterface->all();
		}else{
			return $requestInterface->only($this->onlyFieldsCreate);
		}
    }
    /**
     * @return array
     */
    public function onlyFieldsUpdate($requestInterface)
    {
		$arr = [];
		if(count($this->onlyFieldsUpdate)==0){
			return $requestInterface->all();

		}else{
			foreach($requestInterface->all() as $key => $val){
				if(array_search($key,$this->onlyFieldsUpdate ) !== false){
					$arr[$key] = $val;
				}
			}

			return $arr;
			//return $requestInterface->only($this->onlyFieldsUpdate);
		}
    }


    /**
     * @param $id
     * @param RequestInterface $requestInterface
     * @return Response
     */
    public function show($id, RequestInterface $requestInterface)
    {
		if($id){

            $this->modelQuery->where('id', $id);

			$this->unsetRelations();
			$this->showCallback($id, $requestInterface);

            $item = $this->modelQuery->get()->first();

            if ($item){
				$this->showCallbackByItem($item, $requestInterface);
                return $this->response()->item($item, new $this->transformer)->addMeta('result', 'success');
			}
        }

        return response()->json(['data' => null], 422);
    }

	public function showCallbackByItem($item, $requestInterface){

	}

    /**
     * @param $id
     * @param $requestInterface
     */
	public function showCallback($id, $requestInterface)
    {

	}

    /**
    * @param RequestInterface $requestInterface
    * @return Response
    */
    public function store(RequestInterface $requestInterface)
    {
		$this->unsetRelations();
		if(!$this->storeAccessCheck($requestInterface))
			return response()->json(['data' => null, 'meta' => ['result' => 'error', 'text' => $this->getMessageError()]], 200);


        $item = call_user_func([$this->modelName, 'create'], $this->onlyFieldsCreate($this->request));
        if ($item){
			$this->storeCallback($item);

			return $this->response()->item($item, new $this->transformer)
				->addMeta('text', $this->getMessageSuccess())
				->addMeta('result', 'success');
		}


        return response()->json(['data' => null, 'meta' => ['result' => 'error', 'text' => $this->getMessageError()]], 200);
    }

    /**
     * @param $requestInterface
     * @return bool
     */
	public function storeAccessCheck($requestInterface)
    {
		return true;
	}

    /**
     * @param $item
     */
	public function storeCallback($item){

	}

    /**
    * @param RequestInterface $requestInterface
    * @return Response
    */
    public function update(RequestInterface $requestInterface)
    {
    		$this->unsetRelations();

        $item = $this->modelQuery->where('id', '=', $requestInterface->input('id'))->get()->first(); // firstOrError?

        //TODO: if (!$item) { return [error] }

        if($requestInterface->input('action') == 'changeStatus'){ // бррр ....
            $res = false;
            switch ($requestInterface->input('dataType')){
                case 'bool':
                    if($requestInterface->input($requestInterface->input('fieldName'))){
                        $res = $item->update([
                            $requestInterface->input('fieldName') => 1
                        ]);
                    }else{
                        $res = $item->update([
                            $requestInterface->input('fieldName') => 0
                        ]);
                    }
                    break;
                case 'date':
                    if($requestInterface->input($requestInterface->input('fieldName'))){

                        $res = $item->update([
                            $requestInterface->input('fieldName') => Carbon::now()
                        ]);

                    }else{
                        $res = $item->update([
                            $requestInterface->input('fieldName') => null
                        ]);
                    }
                    break;
            }

            if($res){
                return $this->response()->item($item, new $this->transformer)
                        ->addMeta('text', $this->getMessageSuccess())
                        ->addMeta('result', 'success');
            }
        }else{
            if ($item && $item->update($this->onlyFieldsUpdate($this->request)))
                return $this->response()->item($item, new $this->transformer)
                    ->addMeta('text', $this->getMessageSuccess())
                    ->addMeta('result', 'success');
        }

        return response()->json(['data' => null, 'meta' => ['result' => 'error', 'text' => $this->getMessageError()]], 200);
    }

    /**
    * @param RequestInterface $requestInterface
    * @return mixed
    */
    public function destroy(RequestInterface $requestInterface)
    {
		$this->unsetRelations();
        $item = $this->modelQuery->where('id', '=', $this->request->input('id'))->get()->first();

        if($this->sofDelete){
			if($item && $item->deleted_at){
				$item->delete();
				return response()->json(['data' => null, 'meta' => ['result' => 'success', 'text' => 'Запись успешно уничтожена']], 200);
			}
            if ($item && $item->update([
                'deleted_at' => Carbon::now()
                ]))
                return response()->json(['data' => null, 'meta' => ['result' => 'success', 'text' => $this->getMessageSuccess()]], 200);
        }else{
            if ($item && $item->delete())
                return response()->json(['data' => null, 'meta' => ['result' => 'success', 'text' => $this->getMessageSuccess()]], 200);
        }
        return response()->json(['data' => null, 'meta' => ['result' => 'error', 'text' => $this->getMessageError()]], 200);
    }

    /**
    * @return mixed|string
    */
    public function getActionName()
    {
        return $this->actionName;
    }

    /**
    * @return string
    */
    public function getMessageSuccess()
    {
        switch ($this->getActionName()) {
            case 'store':
                return $this->successCreateText;
            case 'update':
                return $this->successUpdateText;
            case 'destroy':
                return $this->successDeleteText;
        }
    }

    /**
    * @return string
    */
    public function getMessageError()
    {
        switch ($this->getActionName()) {
            case 'store':
                return $this->errorCreateText;
            case 'update':
                return $this->errorUpdateText;
            case 'destroy':
                return $this->errorDeleteText;
        }
    }

    /**
    * @param Request $request
    */
    public function softDeleteCondition(Request $request)
    {
        if($this->sofDelete && !$this->hasField('deleted_at', $request)){

            switch ($this->getActionName()) {
                case 'index':
                    $this->modelTableAlias ? $this->modelQuery->whereNull($this->modelTableAlias . '.deleted_at') : $this->modelQuery->whereNull('deleted_at');
                    break;
                case 'show':
                    break;
                case 'store':
                    break;
                case 'update':
                    break;
                case 'destroy':
                    break;
            }
        }
    }

    /**
    * @param $name
    * @param Request $request
    * @return bool
    */
    public function hasField($name, Request $request)
    {
        if($request->has('fields')){
            foreach ($request->get('fields') as $k => $field){
                if($field['k'] === $name){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     *
     */
    public function prepareFields()
    {
        $model = $this->modelName;
        $fields = $this->request->input('fields');
        $this->modelQuery = $model::where(function ($query) use ($fields) {
            if (is_array($fields)){
                foreach ($fields as $k => $f)
                {
                    if(isset($f['k'])){
                        switch ($f['v']) {
                            case 'notnull':

                             //   $this->modelTableAlias ? $query->whereNotNull($this->modelTableAlias . '.' . $f['k']) : $query->whereNotNull($f['k']);

								$query->whereNotNull($f['k']);
								break;
                            case 'isnull':
                                $this->modelTableAlias ? $query->whereNull($this->modelTableAlias . '.' . $f['k']) : $query->whereNull($f['k']);
                                break;
                            default:
                                $this->modelTableAlias ? $query->where($this->modelTableAlias . '.' . $f['k'], '=', $f['v']) : $query->where($f['k'], '=', $f['v']);
                                break;
                        }
                    }
                }
            }
        });
    }

    /**
    *
    */
    public function prepareWith()
    {
        $model = $this->modelName;
        if($this->request->has('with')){

            $eloquents = [];
            $with = $this->request->input('with');
            foreach ($with as $eloquent) {
              // if (method_exists($model, $eloquent))
                    $model = $this->modelQuery->with($eloquent)->get();
            }
        }
    }

    /**
     *
     */
	public function prepareHas()
    {
        $model = $this->modelName;
        if($this->request->has('has')){
			$orders = $this->request->input('has');
            foreach ($orders as $col => $order) {
				if(is_array($order))
				{
					$col = $order['field'];
					$arr = explode(',', $order['val']);
				}
				else{
					$arr = explode(',', $order);
				}

                $this->modelQuery->whereHas($col, function($query) use ($arr){
                 //   $this->modelTableAlias ? $query->where($this->modelTableAlias . '.' . $arr[1], isset($order['condition']) ? $order['condition'] : '=', $arr[0]) : $query->where($arr[1], '=', $arr[0]);
					$query->where($arr[1], isset($arr[2]) ? $arr[2] : '=', $arr[0]);
                });
            }
        }
    }

    /**
     *
     */
	public function prepareNotIn()
    {
        $model = $this->modelName;
        if($this->request->has('notIn')){
			$notIn = $this->request->input('notIn');
            $this->modelTableAlias ? $this->modelQuery->whereNotIn($this->modelTableAlias . '.id', $notIn) : $this->modelQuery->whereNotIn($this->getTableName() . 'id', $notIn);
        }
    }

    /**
     * @return string
     */
	public function getTableName(){

		return '';
	}

    /**
    * @param $columnListing
    */
    public function prepareOrderBy($columnListing)
    {
        if ($this->request->has('orderby')) {
            $orders = $this->request->input('orderby');
            foreach ($orders as $col => $order) {
                if(in_array($col, $columnListing)){
                    $this->modelTableAlias ? $this->modelQuery->orderBy($this->modelTableAlias . '.' . $col, $order) : $this->modelQuery->orderBy($col, $order);
                    $this->cacheSortByPart .= ":orderby:".$col.":".$order;
                }
            }
        }else{
            $this->defaultOrderBy();
        }
    }

    /**
     * @param $columnListing
     */
    public function prepareGroupBy($columnListing)
    {
        if($this->request->has('groupby')){
            $cols = [];
            $groupby = $this->request->input('groupby');
            foreach($groupby as  $col){
                if(in_array($col, $columnListing))
                    $cols[] = $col;
            }
            if (!empty($cols))
                $this->modelQuery->groupBy(implode(',', $cols));

            if ($this->request->has('having')) {
                $eloquents = [];
                $having = $this->request->input('having');
                foreach ($having as $col => $eloquent) {
                    if (in_array($col, $cols)) {
                        switch (array_keys($eloquent)[0]){
                            case "g": //greater than
                                $this->modelTableAlias ? $this->modelQuery->having($this->modelTableAlias . '.' . $col, '>', $eloquent['g']) : $this->modelQuery->having($col, '>', $eloquent['g']);
                                break;
                            case "l": //less than
                                $this->modelTableAlias ? $this->modelQuery->having($this->modelTableAlias . '.' . $col, '<', $eloquent['l']) : $this->modelQuery->having($col, '<', $eloquent['l']);
                                break;
                            case "e": //equal to
                                $this->modelTableAlias ? $this->modelQuery->having($this->modelTableAlias . '.' . $col, '=', $eloquent['e']) : $this->modelQuery->having($col, '=', $eloquent['e']);
                                break;
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $columnListing
     */
	public function prepareLike($columnListing)
    {
        if($this->request->has('like')){
            $like = $this->request->input('like');
            foreach ($like as $col => $value) {
				if($value != ""){
					if(in_array($col, $columnListing)){
						$this->modelTableAlias ? $this->modelQuery->where($this->modelTableAlias . '.' . $col, 'LIKE', $value) : $this->modelQuery->where($col, 'LIKE', $value);
					}
				}
            }
        }
    }

    /**
     * @param $columnListing
     */
	public function prepareRange($columnListing)
    {
        if($this->request->has('range')){
            $range = $this->request->input('range');
            foreach ($range as $col => $value) {
				$arr = explode(',', $value);
				if($arr[0] != '')
					$this->modelQuery->where($col, '>=', $arr[0]);
				if($arr[1] != '')
					$this->modelQuery->where($col, '<=', $arr[1]);
            }
        }
    }

    /**
     *
     */
    public function defaultOrderBy()
    {
        $this->modelTableAlias ? $this->modelQuery->orderBy($this->modelTableAlias . '.' . 'id', 'DESC') : $this->modelQuery->orderBy('id', 'DESC');
    }

    /**
     * @return string
     */
    public function cacheKeyMainPart()
    {
        $path = $this->request->path();
        $pathParts = explode('/', $path);
        return $pathParts[3].":";
    }



    /**
     * @return string
     */
    public function getCacheKey()
    {
        $key = $this->cacheKeyMainPart();
        if ($this->request->query->get('page')) {
            $key .= "page:" . $this->request->query->get('page');
        }else{
            $key .= "page:0";
        }

        if ($this->cacheSortByPart) {
            $key .= $this->cacheSortByPart;
        }else{
            $key .= ":orderby:id:desc";
        }

        return $key;
    }

    /**
     * @param RequestInterface $requestInterface
     */
    public function extraCondition(RequestInterface $requestInterface)
    {

    }

    /**
     * @param RequestInterface $requestInterface
     */
    public function indexRoleCallback(RequestInterface $requestInterface)
    {

	}

    /**
     * @param $arr
     */
	public function transformTimeRange(&$arr)
    {
		for($i = 0; $i < count($arr); $i++){
			$arr[$i]['created_at'] = (strlen($arr[$i]['hour']) == 1) ? '0'.$arr[$i]['hour'].':00' : $arr[$i]['hour'].':00';
		}
	}

    /**
     * @return array
     */
	public function generateTimeRange(){
		$times = [];
		for($i=0; $i<24; $i++){
			$times[] = ['created_at'=>(strlen($i) == 1) ? '0'.$i.':00' : $i.':00', 'sum_with_discount' => 0];
		}
		return $times;
	}

    /**
     * @param Carbon $start_date
     * @param Carbon $end_date
     * @return array
     */

    /**
     * @param $dates
     * @param $time
     * @return int|string
     */
	public function getKeyByTime($dates, $time)
	{
		foreach ($dates as $k => $date){
			if($date['created_at'] === $time){
				return $k;
			}
		}
	}

	public function getKeyByDate($dates, Carbon $dateForKey)
	{
		foreach ($dates as $k => $date){
			if($date['created_at'] === $dateForKey->format('d.m.Y')){
				return $k;
			}
		}
	}


	public function generateDateRange(Carbon $start_date, Carbon $end_date)
	{
		$dates = [];

		for($date = $start_date; $date->lte($end_date); $date->addDay()) {
			$dates[] = ['created_at' => $date->format('d.m.Y'), 'sum_with_discount' => 0];
		}

		return $dates;
	}

	/**
	 * @param $dates
	 * @param Carbon $dateForKey
	 * @return int|string
	 */
    /**
     * @param $requestInterface
     */
    public function prepareTransformers($requestInterface)
    {
        if($requestInterface->has('unset_relations')){
            foreach ($requestInterface->get('unset_relations') as $modelName => $relations){
                /**
                 * @var BaseModel $class
                 */
                $class = 'App\\Models\\' . $modelName;

                foreach ($relations as $relation){
                    unset($class::$relationModels[$relation]);
                }
            }
        }
    }

}
