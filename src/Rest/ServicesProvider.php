<?php
namespace Rest\Services;

use Illuminate\Support\ServiceProvider;
use File;

class ServicesProvider extends ServiceProvider
{
    /**
     * 
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/rest.php' => app_path().'/../config/rest.php',
            __DIR__.'/Api/V1/Controllers/RestController.php' => app_path().'/Api/V1/Controllers/RestController.php',
        ]);
    }
    
    /**
     * Register the service provider.
     * Merge config, load routes
     *
     * @return void
     */
    public function register()
    {
		
    }
}
